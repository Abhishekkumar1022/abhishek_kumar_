//Anagrams
/*Given two strings,write a function to determine if the second string is the anagram of the first.A anagram is a word phrase or name formed by rearranging the letters of another,such as cinema,formed from iceman.
validAnagram('','') //true
validAnagram('aaz','zza') //false
*/

/*
function validAnagram(str1,str2){

    if(str1.length!=str2.length){
      return false;
    }
  
    let str1Frequency={}
    let str2Frequency={}
  
    for(let val of str1){
      str1Frequency[val]=(str1Frequency[val] ||0)+1
    }
    for(let val of str2){
      str2Frequency[val]=(str2Frequency[val] ||0)+1
    }
    console.log(str1Frequency)
    console.log(str2Frequency)
    for(let key in str1Frequency){
        if(!(key in str2Frequency)){
          return false;
        }
        if((str2Frequency[key]!=str1Frequency[key])){
          return false;
        }
      }
      return true;
  }
  validAnagram('aaz','zza')
  */
 //Better Solution

 function validAnagram(first, second) {
  if (first.length !== second.length) {
    return false;
  }

  const lookup = {};

  for (let i = 0; i < first.length; i++) {
    let letter = first[i];
    // if letter exists, increment, otherwise set to 1
    lookup[letter] ? lookup[letter] += 1 : lookup[letter] = 1;
  }
  console.log(lookup)

  for (let i = 0; i < second.length; i++) {
    let letter = second[i];
    // can't find letter or letter is zero then it's not an anagram
    if (!lookup[letter]) {
      return false;
    } else {
      lookup[letter] -= 1;
    }
  }

  return true;
}

// {a: 0, n: 0, g: 0, r: 0, m: 0,s:1}
validAnagram('anagrams', 'nagaramm')