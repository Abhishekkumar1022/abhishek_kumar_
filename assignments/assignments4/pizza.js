var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Pizza = /** @class */ (function () {
    function Pizza(base, size) {
        this.base = base;
        this.size = size;
    }
    return Pizza;
}());
var Veg = /** @class */ (function (_super) {
    __extends(Veg, _super);
    function Veg(base, size) {
        return _super.call(this, base, size) || this;
    }
    Veg.prototype.priceCalculator = function (topping) {
        var price = '';
        //console.log(topping);
        if (topping == 'Mozzerella') {
            price = 500;
            console.log('Calculating price for Veg Pizza');
        }
        else {
            price = 400;
        }
        console.log('Price of Veg Pizza with ' + topping + ' topping is Rs ' + price);
        return price;
    };
    return Veg;
}(Pizza));
var NonVeg = /** @class */ (function (_super) {
    __extends(NonVeg, _super);
    function NonVeg(base, size) {
        return _super.call(this, base, size) || this;
    }
    NonVeg.prototype.priceCalculator = function (topping) {
        var price = '';
        if (topping == 'Cheese') {
            price = 700;
            console.log('Calculating price for Non-Veg Pizza');
        }
        else {
            price = 600;
        }
        console.log('Price of Non-Veg Pizza with ' + topping + ' topping is Rs ' + price);
        return price;
    };
    return NonVeg;
}(Pizza));
var vegpiz = new Veg('Thin crust', 'medium');
var nonvegpiz = new NonVeg('Thin crust', 'medium');
vegpiz.priceCalculator('Mozzerella');
nonvegpiz.priceCalculator('Cheese');
