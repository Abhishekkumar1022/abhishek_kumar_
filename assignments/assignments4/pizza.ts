class Pizza{
    
    constructor(private base:string,private size:string){
        
    }
    
}
class Veg extends Pizza{
    constructor(base:string,size:string){
        super(base,size);
    }
    
    priceCalculator(topping:string){
        var price:any='';
        //console.log(topping);
        if(topping=='Mozzerella'){
            price=500;
            console.log('Calculating price for Veg Pizza');
        }
        else{
            price=400;
        }

        console.log('Price of Veg Pizza with '+topping+' topping is Rs '+price);
        return price;
    }
    

}
class NonVeg extends Pizza{
    constructor(base:string,size:string){
        super(base,size);
    }
    priceCalculator(topping:string){
        var price:any='';
        if(topping=='Cheese'){
            price=700;
            console.log('Calculating price for Non-Veg Pizza');
        }
        else{
            price=600;
        }

        console.log('Price of Non-Veg Pizza with '+topping+' topping is Rs '+price);
        return price;
    }
}



var vegpiz=new Veg('Thin crust','medium');
var nonvegpiz= new NonVeg('Thin crust','medium');
vegpiz.priceCalculator('Mozzerella');
nonvegpiz.priceCalculator('Cheese');

