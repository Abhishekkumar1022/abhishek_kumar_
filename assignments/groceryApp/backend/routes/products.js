const express=require('express')
const router=express.Router()

router.get('/',(req,res)=>{
    res.send({
        products: [{"id":1,"name":"LED TV","price":10000,"description":"Sensex 80 cm (32 Inches) Full HD LED TV SX320 (Black) (2019 Model)","imgLoc":"../../../assets/images/ledtv.jpg","quantity":1},
        {"id":2,"name":"IPHONE XR","price":70000,"description":"Apple iPhone XR (64GB) - White","imgLoc":"../../../assets/images/iphonexr.jpg","quantity":1},
        {"id":3,"name":"ALEXA","price":6000,"description":"Echo Dot (3rd Gen) – New and improved smart speaker with Alexa (Black)","imgLoc":"../../../assets/images/alexa.jpg","quantity":1},
        {"id":4,"name":"VR HEADSET","price":80000,"description":"Oculus Quest All-in-One VR Gaming System - 64GB (128 GB)","imgLoc":"../../../assets/images/vr.jpg","quantity":1},
        {"id":5,"name":"BOAT SPEAKER","price":1299,"description":"boAt Stone 200 Portable Bluetooth Speakers (Black)","imgLoc":"../../../assets/images/speaker.jpg","quantity":1},
        {"id":6,"name":"LAPTOP","price":32999,"description":"HP 14 Core i3 7th gen 14-inch Thin and Light Laptop (8GB/256GB SSD/Windows 10 Home/MS Office/Jet Black/1.43 kg), 14q-cs0023TU","imgLoc":"../../../assets/images/laptop.jpg","quantity":1},
        {"id":7,"name":"PRINTER","price":12999,"description":"Epson L 3152 WiFi All in One Ink Tank Printer","imgLoc":"../../../assets/images/printer.jpg","quantity":1},
        {"id":8,"name":"AIRPOD","price":2500,"description":"boAt Airdopes 311v2 True Wireless Earbuds (Bluetooth V5.0) with HD Sound and Sleek Design, Integrated Controls with in-Built Mic and 500mAh Charging Case","imgLoc":"../../../assets/images/airpod.jpg","quantity":1},
        {"id":9,"name":"AIR PUFIED","price":29900,"description":"Dyson Pure Cool Link Tower WiFi-Enabled Air Purifier, TP03 (White/Silver)","imgLoc":"../../../assets/images/airpurifier.jpg","quantity":1},
        {"id":10,"name":"IPAD","price":29900,"description":"Apple iPad (10.2-inch, Wi-Fi, 32GB) - Space Grey (Latest Model)","imgLoc":"../../../assets/images/ipad.jpg","quantity":1}
    ],
      })
})

/*
  app.route('/api/getcartdata').post((req, res) => {
  res.send(201, req.body)
})
  */

module.exports=router;