const express = require('express')
const app = express()
const cors = require('cors')
const mongoose=require('mongoose')
const morgan=require('morgan')
const bodyParser = require('body-parser')
const MongoClient = require('mongodb').MongoClient;
const uri='mongodb+srv://admin:'+process.env.MONGO_ATLAS_PW+'@cluster0-l4m4x.mongodb.net/test?retryWrites=true&w=majority'

//Using cors for cross-origin-request
var corsOptions = {
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200  
}
app.use(cors(corsOptions))


//Adding Routes
const getProductRoute=require('./routes/products')
//app.use('/api/getproducts',getProductRoute)
app.get('/getproduct',(req,res)=>{
  console.log("get product call");
  
const client = new MongoClient(uri, { useNewUrlParser: true });
client.connect(err => {
  const collection = client.db("shop").collection("products");
  console.log('Db connected');
  collection.find().toArray((err,data)=>{
  client.close();
  res.send(data);
  });
  // perform actions on the collection object
  
});


})
//Connecting to mongoDB(Using MongoAtlas)
/*
mongoose.connect('mongodb+srv://admin:'+process.env.MONGO_ATLAS_PW+'@cluster0-l4m4x.mongodb.net/test?retryWrites=true&w=majority',{
  useMongoClient:true
})
*/

app.use(bodyParser.json())
app.use(morgan('dev'))

app.listen(8000, () => {
  console.log('Server started! at port 8000')
})