import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  
  constructor(private cartService:CartService) { }
  private cartData:any;
  private cartItems=[];
  private total:number;
  private subTotal:any=0;
  private quantity:number=1;
  
  ngOnInit() {
    this.cartData=this.cartService.getCartData();
    this.cartItems=Object.keys(this.cartData);
    this.getSubTotal(this.cartItems);
  }
  
  getSubTotal(cartItemsid){
    //console.log(cartItemsid);
    cartItemsid.forEach(element => {
      this.subTotal+=this.cartData[element].price;
    });
    //console.log(this.subTotal);
  }
  getTotalAmount(ev,id){
    this.quantity=ev.target.value;
    this.total=this.cartData[id].price*this.quantity;
    //console.log(this.cartData[id].price);
    this.subTotal+=this.total;
  }
  checkout(){
    console.log('checkout done');
    this.cartService.checkout(this.cartData,this.quantity,this.subTotal);
  }


}

