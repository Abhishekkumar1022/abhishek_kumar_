import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../services/products.service';
import { CartService } from '../../services/cart.service';
declare var $:any;

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private productservice:ProductsService,private cartservice:CartService) { }

  public products:any=[];
  hidebutton: any[] = [];

  
  ngOnInit() {
    this.productservice.getProducts().subscribe(data=>this.products=data);
  }
  
  //calls addCartData in CartService
  addItems(ev,product,id){
    this.cartservice.addCartData(product);
    this.hidebutton[id] = true;
  }
  
  //calls removeCartData in CartService
  removeItems(ev,product,id){
  this.cartservice.removeCartData(product);
  this.hidebutton[id] = false;
  }


}
