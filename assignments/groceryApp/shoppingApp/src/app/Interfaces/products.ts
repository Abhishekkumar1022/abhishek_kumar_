//Interface for Products
export interface Products {
    id:number;
    name:string;
    price:number;
    description:string;
    imgLoc:string;
}
