import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './Components/cart/cart.component'
import { HomepageComponent } from './Components/homepage/homepage.component';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { CheckoutComponent } from './Components/checkout/checkout.component';

//Routes for all the components
const routes: Routes = [
  {path: 'cart' , component: CartComponent},
  {path: 'checkout' , component: CheckoutComponent},
  {path: 'home' , component: HomepageComponent},
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
