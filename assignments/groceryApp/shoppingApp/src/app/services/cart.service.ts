import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ProductsService } from './products.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  
  constructor() { }

  cartdata={};
  getCartData(){
    return this.cartdata;
  }

  //adds data to the cart
  addCartData(product){
    this.cartdata[product.id]={
      ...product,
      quantity:1,
      totalPrice:product.price
    }
  }

  //Removes data from the cart
  removeCartData(product){
    delete this.cartdata[product.id];
  }
  checkout(cartdata,quantity,subTotal){
  }
}
