import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Products } from '../Interfaces/products';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProductsService {
  //Url declared for json file
  //private url='/assets/data/products.json'
  private url='http://localhost:8000/api/getproducts'

  constructor(private http:HttpClient) { }
  
  //get Products List as Observables
  getProducts():Observable<Products[]>{
    return this.http.get<Products[]>(this.url);
  }

}

