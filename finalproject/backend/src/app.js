const express = require('express')
const cors = require('cors')
const userRouter = require('./routers/user')
const userAppartment = require('./routers/appartments')
const port = process.env.PORT
require('./db/db')

const app = express()

app.use(express.json())
app.use(cors())
app.use(userRouter)
app.use(userAppartment)
app.listen(port, () => {
    console.log(`Server running on port ${port}`)
})
