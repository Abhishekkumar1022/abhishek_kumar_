const mongoose = require('mongoose')

const appartmentsSchema=new mongoose.Schema({
    ap_id:{ 
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    },
    desc:{
        type:String,
        required:true
    },
    imgloc:{
        type:String,
        required:true
    },
    price:{
        type:String,
        required:true
    },
    location:{
        type:String,
        required:true
    }
})

const appartments = mongoose.model('appartments', appartmentsSchema)

module.exports = appartments