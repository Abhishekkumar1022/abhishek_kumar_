const express = require('express');
const appartments = require('../models/appartments');
const router = express.Router()

router.get('/api/v1/appartment-list', async (req, res) => {
  const appartment = await appartments.find({});

  try {
    res.send(appartment);
  } catch (err) {
    res.status(500).send(err);
  }
});

module.exports = router