import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { AppartmentService } from '../../shared/appartment.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userInfo:any;
  public appartments:any=[];


  constructor(public authService: AuthService,private appartmentservice:AppartmentService) { }

  ngOnInit() {
    this.authService.getUserProfile().subscribe((res)=>{
      this.userInfo=res;
    });

    this.appartmentservice.getAppartments().subscribe((res)=>{
      this.appartments=res;
      console.log(this.appartments);
    });
  
  }

}
