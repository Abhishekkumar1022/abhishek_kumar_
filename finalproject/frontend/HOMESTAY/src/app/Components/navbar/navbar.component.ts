import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedin:boolean=false;
  isshow:boolean=true;

  constructor(public authService: AuthService,public router: Router) { }
  
  //when logged in only shows logout
  ngOnInit() {
    this.isLoggedin=this.isLoggedIn();
    if(this.isLoggedin==true){
      this.isshow=false;
    }
    else{
      this.isshow=true;
    }
  }
  
  //checks if user is logged in
  isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    return (authToken !== null) ? true : false;
  }
  
  //log out 
  logout(){
    this.authService.doLogout();
    this.router.navigate(['login']);
  }

}
