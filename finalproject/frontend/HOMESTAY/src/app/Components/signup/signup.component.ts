import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from './../../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  Roles: any = ['Host', 'Guest'];

  constructor(public authService: AuthService,
    public router: Router) { }

  ngOnInit() {
  }
  form: FormGroup = new FormGroup({
    name: new FormControl(''),
    password: new FormControl(''),
    email   : new FormControl('')
  });
  @Input() error: string | null;

  @Output() submitEM = new EventEmitter();

  submit() {
    if (this.form.valid) {
      this.submitEM.emit(this.form.value);
      console.log(this.form.value);
      if(this.form.value.name==""||this.form.value.password=="" ||this.form.value.email==""){
        this.error='Username,Email,Password or Role Cannot be Empty';
      }
      else{
        this.authService.signUp(this.form.value).subscribe((res) => {
          if (res.result) {
            this.form.reset()
            this.router.navigate(['login']);
          }
        })
      }
      }
    }
  }



