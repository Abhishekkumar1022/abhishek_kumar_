import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(public authService: AuthService) { }
  userInfo:any;

  ngOnInit() {
    this.authService.getUserProfile().subscribe((res)=>{
      this.userInfo=res;
    });
  }

}
