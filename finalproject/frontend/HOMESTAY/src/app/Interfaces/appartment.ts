//Interface for Appartments
export interface Appartment {
    _id:string
    ap_id:string;
    name:string;
    desc:string;
    imgLoc:string;
    price:string;
    location:string;
}
