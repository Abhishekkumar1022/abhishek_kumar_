import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  name:string="Abhishek"
  email:string='ak87726@gmail.com'
  age:number=21;

  constructor() { }

  ngOnInit() {
  }

}
